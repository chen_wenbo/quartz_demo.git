# quartz_demo

#### 介绍
使用quartz实现任务调度定时任务，包含任务的启停，一次执行，支持corn表达式。
支持基于内存实现和数据库实现。

#### 软件架构
软件架构说明


#### 安装教程

1.  git clone https://gitee.com/chen_wenbo/quartz_demo.git
2.  导入数据库，在resource/sql/quartz.sql
3.  给User表任意添加一条数据
4.  启动

#### 使用说明

1.  localhost:9999/quartz/execute  立即执行一次
2.  localhost:9999/quartz/beginExecute  启动定时任务，quartz任务开启
3.  localhost:9999/quartz/pause  暂停定时任务，quartz任务停止
4.  localhost:9999/quartz/update  修改定时任务，如果为开启状态：添加quartz任务，如果为停止状态：删除quartz任务
5.  localhost:9999/quartz/delete  删除定时任务，任务quartz停止
6.  localhost:9999/quartz/add  添加定时任务，如果状态为开启状态：任务quartz开启，如果状态为停止状态：任务quartz关闭
