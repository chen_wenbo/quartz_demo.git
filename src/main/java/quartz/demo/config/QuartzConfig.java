package quartz.demo.config;

import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import quartz.demo.entity.QuartzBean;
import quartz.demo.service.QuartzService;
import quartz.demo.test.QuartzTest;

import javax.annotation.Resource;

/**
* @Description: 基于内存的定时任务调度
* @Date: 2023-04-17 14:51:37
* @Author: 陈文波
*/
//@Configuration
public class QuartzConfig {

    @Resource
    private QuartzService quartzService;

//    @Bean
//    public JobDetail testQuartz2() {
//        QuartzBean byId = quartzService.getById(1);
//        System.out.println("byId = " + byId.getCode());
//        return JobBuilder.newJob(QuartzTest.class).withIdentity("testTask2").storeDurably().build();
//    }
//
//    /**
//     * 触发器，该方法使用cron触发器
//     * @return
//     */
//    @Bean
//    public Trigger testQuartzTrigger2() {
//        //cron方式，每隔5秒执行一次
//        return TriggerBuilder.newTrigger().forJob(testQuartz2())
//                .withIdentity("testTask2")
//                .withSchedule(CronScheduleBuilder.cronSchedule("*/5 * * * * ?"))
//                .build();
//    }


}
