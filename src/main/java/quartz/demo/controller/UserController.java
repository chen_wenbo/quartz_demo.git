package quartz.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import quartz.demo.entity.User;
import quartz.demo.mapper.UserMapper;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class UserController {

    @Resource
    private UserMapper userMapper;


    @GetMapping("/getUser")
    public String getUser(){
        List<User> users = userMapper.selectList(null);
        return users.toString();
    }

}
