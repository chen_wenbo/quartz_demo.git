package quartz.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import quartz.demo.entity.QuartzBean;

@Mapper
public interface QuartzMapper extends BaseMapper<QuartzBean> {
}
