package quartz.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;
import quartz.demo.entity.QuartzBean;


public interface QuartzService extends IService<QuartzBean> {

    void execute(QuartzBean quartzBean) throws Exception;

    void beginExecute();

    void pause(QuartzBean quartzBean);

    void updateJob(QuartzBean quartzBean) throws SchedulerException;

    void delete(int id);

    void add(QuartzBean quartzBean);
}
