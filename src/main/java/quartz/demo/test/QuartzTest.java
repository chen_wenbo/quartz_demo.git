package quartz.demo.test;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import quartz.demo.controller.UserController;

import javax.annotation.Resource;

public class QuartzTest implements Job {

    private String parameter;

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Resource
    private UserController userController;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        String user = userController.getUser();
//        System.out.println("jobExecutionContext = " + jobExecutionContext.getJobDetail().getJobClass());
        System.out.println("user = " + user);

    }




}
